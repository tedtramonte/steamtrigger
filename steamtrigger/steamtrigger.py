import json
import logging
import os
import sys

import asyncio
import backoff
import requests
import websockets


logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='[%Y-%m-%d] %H:%M:%S -', level=logging.getLevelName(os.getenv('LOG_LEVEL') or 'INFO'))

current_build_id = ''


@backoff.on_exception(backoff.expo,
                      (requests.exceptions.ConnectionError, requests.HTTPError,
                       json.decoder.JSONDecodeError, TypeError),
                      max_time=300,
                      max_tries=20,
                      jitter=backoff.full_jitter)
def get_current_build_id(app_id):
    logging.debug('Getting Steam API build id...')
    api_base_url = os.getenv('API_BASE_URL') or 'https://api.steamcmd.net/'
    try:
        r = requests.get(f'{api_base_url}v1/info/{app_id}')
        logging.debug(r.text)
        logging.info('Connected to API.')
    except requests.exceptions.ConnectionError:
        logging.error('Connection to API failed.')
        raise

    try:
        r.raise_for_status()
        logging.info('Request to API succeeded.')
    except requests.HTTPError:
        logging.error('Request to API failed.')
        raise

    try:
        steam_data = r.json()
        logging.info('Successfully parsed Steam data.')
    except json.decoder.JSONDecodeError:
        logging.error('Parsing Steam data resulted in malformed JSON.')
        raise

    try:
        build_id = steam_data['data'][app_id]['depots']['branches']['public']['buildid']
        logging.info(f'Current build id found: {build_id}')
    except TypeError:
        logging.error('Failed to get current build id.')
        raise

    return build_id


async def handler(websocket, app_id, trigger_url, trigger_token, trigger_ref):
    global current_build_id
    async for message in websocket:
        message_data = json.loads(message)
        try:
            for k, v in message_data['Apps'].items():
                logging.debug(f'Message\'s app id is: {k}')
                if k == app_id:
                    logging.info('Relevant changenumber event received!')
                    logging.info(f'Giving the API some time to update for app {app_id}...')
                    await asyncio.sleep(180)
                    logging.debug(f'Current build id for app {app_id}: {current_build_id}')
                    new_build_id = get_current_build_id(app_id)
                    logging.debug(f'Found build id {new_build_id} comapred to current build id {current_build_id}')
                    if new_build_id > current_build_id:
                        # The changenumber indicated a new build
                        logging.info('New build id detected! Triggering pipeline...')
                        current_build_id = new_build_id
                        logging.debug(f'New current build id is {current_build_id}')
                        payload = {
                            'token': trigger_token,
                            'ref': trigger_ref,
                            'variables[STEAM_BUILD_ID]': current_build_id,
                            'variables[TAG_LATEST]': 'true',
                        }
                        r = requests.post(trigger_url, data=payload)
                        logging.debug(r.text)
                        try:
                            r.raise_for_status()
                            logging.info('Pipeline triggered.')
                        except requests.HTTPError:
                            logging.error('Pipeline trigger failed.')
                            raise
        except KeyError:
            pass


async def listen(url):
    logging.info(f'Listening to {url} for Steam changenumber events...')
    async with websockets.connect(url) as websocket:
        await handler(websocket, os.getenv('APP_ID'), os.getenv('TRIGGER_URL'),
                      os.getenv('TRIGGER_TOKEN'), os.getenv('TRIGGER_REF'))


def main():
    try:
        global current_build_id
        current_build_id = get_current_build_id(os.getenv('APP_ID'))
        logging.debug(f'Initial build id: {current_build_id}')
        asyncio.run(listen(os.getenv('WEBSOCKET_URL') or 'ws://localhost:8181/'))
    except (requests.exceptions.ConnectionError, requests.HTTPError,
            json.decoder.JSONDecodeError, TypeError):
        logging.error('Couldn\'t automatically resolve an error by retrying.')
        logging.error('Exiting.')
        sys.exit()


if __name__ == '__main__':
    main()
