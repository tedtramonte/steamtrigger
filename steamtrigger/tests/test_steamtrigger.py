from steamtrigger.steamtrigger import get_current_build_id


class TestGetCurrentBuildId:
    def test_succeeds(self):
        assert get_current_build_id('896660')
