# Steamtrigger
Steamtrigger is an  OCI container image for triggering a GitLab CI/CD Pipeline based on Steam application activity. The underlying script is written in Python and relies on a Steam changenumber websocket from [xPaw's SteamWebPipes](https://github.com/xPaw/SteamWebPipes) and Steam data lookups fron [Jona Koudijs' https://www.steamcmd.net/](https://www.steamcmd.net/).

You would use this if you needed to run a GitLab Pipeline any time a Steam application is updated. You can see it in action at [gitlab.com/tedtramonte/valheim-server](https://www.gitlab.com/tedtramonte/valheim-server) where a new image is built for every build of the game's server.

Currently, Steamtrigger is hardcoded to watch for one Steam `app id`'s public branch and activate one GitLab Pipeline Trigger. Eventually I'd like to see it configurable to as many apps, branches, and triggers as desired. For now, I've spent more time putting together the supporting services for [valheim-server](https://www.gitlab.com/tedtramonte/valheim-server) than I have playing the game.

## Requirements
- An OCI compliant container engine like Docker or Podman
- A websocket publishing Steam changenumber events
  - [SteamWebPipes](https://www.gitlab.com/tedtramonte/SteamWebPipes) is an OCI container image serving this purpose
- A GitLab Pipeline Trigger url and corresponding token

## Usage
```bash
# Assuming you already have a SteamWebPipes running on localhost:8181
docker run -d -e "APP_ID='896660'" -e "TRIGGER_TOKEN='12312412uhajsdbasdf1234'" -e "TRIGGER_URL='https://gitlab.com/api/v4/projects/12398124091275/trigger/pipeline'" tedtramonte/steamtrigger:latest

# If your SteamWebPipes url is different
docker run -d -e "APP_ID='896660'" -e "TRIGGER_TOKEN='12312412uhajsdbasdf1234'" -e "TRIGGER_URL='https://gitlab.com/api/v4/projects/12398124091275/trigger/pipeline'" -e "WEBSOCKET_URL='ws://steamwebpipes:8282/'" tedtramonte/steamtrigger:latest
```

If you're starting from scratch, you're probably better off using `docker-compose`. This will start up `steamtrigger`, `steamwebpipes`, and `steamcmd_api`.
```yaml
version: "3.8"
services:
  trigger:
    image: tedtramonte/steamtrigger:latest
    restart: unless-stopped
    environment:
      API_BASE_URL: http://steamcmd_api:8080/
      APP_ID: '896660'
      # TRIGGER_REF: my-custom-branch
      TRIGGER_TOKEN: 12312412uhajsdbasdf1234
      TRIGGER_URL: https://gitlab.com/api/v4/projects/12398124091275/trigger/pipeline
      WEBSOCKET_URL: ws://steamwebpipes:8181/

  # This is optional, but the official steamcmd api is spotty
  # and this ensures your steamtriggers can read an api
  steamcmd_api:
    image: steamcmd/api:latest
    restart: unless-stopped
    expose:
      - "8080"

  steamwebpipes:
    image: tedtramonte/steamwebpipes:latest
    restart: unless-stopped
    # Uncomment below if you need to expose SteamWebPipes on the host too
    # ports:
    #   - "8181:8181"
```

## Configuration
### Environment Variables
- `API_BASE_URL` - The URL of the steamcmd API to check for (default: `https://api.steamcmd.net/`)
- `APP_ID` - The app ID of the application to watch for
- `LOG_LEVEL` - The level of logging to output (default: `INFO`)
- `TRIGGER_REF` - The Git ref where the pipeline should run (default: `main`)
- `TRIGGER_TOKEN` - The GitLab Trigger token
- `TRIGGER_URL` - The GitLab Trigger url
- `WEBSOCKET_URL` - The URL of the websocket to listen to for Steam changenumber events (default: `ws://localhost:8181/`)

## Contributing
Merge requests are welcome after opening an issue first.
