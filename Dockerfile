FROM python:3.11-alpine@sha256:25df32b602118dab046b58f0fe920e3301da0727b5b07430c8bcd4b139627fdc
ENV PYTHONBUFFERED 1
ENV API_BASE_URL https://api.steamcmd.net/
ENV LOG_LEVEL 'INFO'
ENV WEBSOCKET_URL ws://localhost:8181/
ENV TRIGGER_REF main
WORKDIR /steampipe
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . ./
ENTRYPOINT ["python", "-u", "./steamtrigger/steamtrigger.py"]
