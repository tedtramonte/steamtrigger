## 3.0.0 (2022-07-14)

### Feat

- add TRIGGER_REF env var

### BREAKING CHANGE

- Default ref changed from master to main

## 2.2.4 (2022-06-29)

### Fix

- handle ConnectionError and add more logging
- remove deprecated asyncio function

## 2.2.3 (2022-01-25)

## 2.2.2 (2022-01-25)

### Fix

- specify the default values in code

## 2.2.1 (2022-01-25)

### Fix

- remove silly global usage

## 2.2.0 (2022-01-25)

### Feat

- add API_BASE_URL environment config

## 2.1.4 (2022-01-23)

## 2.1.3 (2021-06-09)

### Fix

- attempt to fix build_id reporting equal again

## 2.1.2 (2021-03-31)

### Fix

- attempt to fix build_id incorrectly reporting equal

## 2.1.1 (2021-03-23)

### Fix

- adjust initial current_build_id definition to allow for unit tests

## 2.1.0 (2021-03-23)

### Feat

- add LOG_LEVEL environment variable

### Fix

- ensure current_build_id is global

## 2.0.0 (2021-03-02)

### Feat

- implement jitter backoff retry on requests

### BREAKING CHANGE

- Function parameters were added across to the board to increase testability

## 1.0.0 (2021-02-05)

### Feat

- initial commit
